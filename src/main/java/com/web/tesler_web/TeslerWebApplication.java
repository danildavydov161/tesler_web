package com.web.tesler_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeslerWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeslerWebApplication.class, args);
    }

}
